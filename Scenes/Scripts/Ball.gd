extends RigidBody2D

export var speedup = 4;
const MAXSPEED = 400;

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	set_physics_process(true)

func _physics_process(delta):
	# Called every frame. Delta is time since last frame.
	# Update game logic here.
	var bodies = get_colliding_bodies()
	
	for body in bodies:
		if body.is_in_group("Bricks"):
			get_node("/root/World").score += 5
			body.queue_free()
		
		if body.get_name() == "Paddle":
			var speed = get_linear_velocity().length()
			var direction = position - body.get_node("Anker").get_global_position()
			var velocity = direction.normalized() * min(speed + speedup, MAXSPEED)
			
			set_linear_velocity(velocity)
			print(str(speed+speedup))
			
	# if ball out of bottom, kill ball
	if position.y > get_viewport_rect().end.y:
		queue_free()